﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Grafico
    {
        public decimal Valor { set; get; }
        public string Fecha { set; get; }
    }
}
