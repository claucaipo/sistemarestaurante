﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
namespace SistemaRestaurante.BusinessEntity
{
    public class Producto
    {
        public int IdProducto { set; get; }
        public string  Nombre { set; get; }
        public string Descripcion { set; get; }
        public string Imagen { set; get; }
        public int IdCategoria { set; get; }
        public decimal Precio { set; get; }

        public IList<Categoria> Categorias { set; get; }

        public string NombreCategoria { set; get; }

        public HttpPostedFileBase File { set; get; }
    }
}
