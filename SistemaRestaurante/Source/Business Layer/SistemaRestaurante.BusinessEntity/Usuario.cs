﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace SistemaRestaurante.BusinessEntity
{
    public class Usuario
    {
        public int IdUsuario { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "Maximo 50 caracteres en el ususario")]
        public string Username { get; set; }

        [Required]
        [StringLength(50,ErrorMessage = "Maximo de 50 caracteres en el password")]
        public string Password { set; get; }

        [Required]
        public int Rol { set; get; }

        [DataType(DataType.EmailAddress)]
        public string Email { set; get; }
        
        public int IdCliente { set; get; }

        [DataType(DataType.DateTime)]
        public string HoraRegistro { set; get; }

        public bool RememberMe { set; get; }
    }
}
