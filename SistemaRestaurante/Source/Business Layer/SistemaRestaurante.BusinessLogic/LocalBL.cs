﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.BusinessLogic
{
    using SistemaRestaurante.BusinessEntity;
    using SistemaRestaurante.DataAcess;

    public class LocalBL : Singleton<LocalBL>
    {
        public IList<Local> Listar()
        {
            try
            {
                return LocalDAL.Instancia.Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Agregar(Local local)
        {
            try
            {
                return LocalDAL.Instancia.Agregar(local);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Local local)
        {
            try
            {
                return LocalDAL.Instancia.Modificar(local);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar(int idLocal)
        {
            try
            {
                return LocalDAL.Instancia.Eliminar(idLocal);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Local Obtener(int idLocal)
        {
            try
            {
                return LocalDAL.Instancia.Obtener(idLocal);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }   
 
        public int Count(string where)
        {
            try
            {
                return LocalDAL.Instancia.Count(where);
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
        }

          public IList<Local> Listar(string sordColumn, string sordDirection, int rowsCount, int page, int count, string where)
          {
              try
              {
                  return LocalDAL.Instancia.Listar(sordColumn, sordDirection, rowsCount, page, count, where);
              }
              catch (Exception ex)
              {

                  throw new Exception(ex.Message);
              }
          }
      
    }
}
