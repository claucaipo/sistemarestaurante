﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.BusinessLogic
{
    using SistemaRestaurante.BusinessEntity;
    using SistemaRestaurante.DataAcess;

    public class ProductoBL : Singleton<ProductoBL>
    {
        public IList<Producto> Listar()
        {
            try
            {
                return ProductoDAL.Instancia.Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int Agregar(Producto producto)
        {
            try
            {
                return ProductoDAL.Instancia.Agregar(producto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Modificar(Producto producto)
        {
            try
            {
                return ProductoDAL.Instancia.Modificar(producto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Eliminar(int idProducto)
        {
            try
            {
                return ProductoDAL.Instancia.Eliminar(idProducto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Producto Obtener(int idProducto)
        {
            try
            {
                return ProductoDAL.Instancia.Obtener(idProducto);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

    }
}
