﻿
namespace SistemaRestaurante.Utils.Enums
{
    public enum TipoEstado : int
    {
        Inactivo = 0,
        Activo = 1
    }
}
