﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SistemaRestaurante.BusinessEntity;

namespace SistemaRestaurante.DataAcess
{
    public class CategoriaDAL : Singleton<CategoriaDAL>
    {
        private readonly Database BaseDatos = DatabaseFactory.CreateDatabase();

        public IList<Categoria> Listar()
        {
            var categorias = new List<Categoria>();
            var comando = BaseDatos.GetStoredProcCommand("usp_SelectAll_Categoria");

            using (var lector= BaseDatos.ExecuteReader(comando))
            {
                while(lector.Read())
                {
                    categorias.Add( new Categoria
                                        {
                                            IdCategoria = lector.GetInt32(lector.GetOrdinal("IdCategoria")),
                                            Nombre = lector.GetString(lector.GetOrdinal("Nombre")),
                                            Imagen = lector.GetString(lector.GetOrdinal("Imagen"))
                                        });
                }
            }
            comando.Dispose();
            return categorias;
        }
        
        public Categoria Obtener (int idCategoria)
        {
            var categoria = new Categoria();
            var comando = BaseDatos.GetStoredProcCommand("usp_Select_Categoria");
            BaseDatos.AddInParameter(comando,"IdCategoria",DbType.Int32, idCategoria);
            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                if(lector.Read())
                {
                    categoria.IdCategoria = lector.GetInt32(lector.GetOrdinal("IdCategoria"));
                    categoria.Nombre = lector.GetString(lector.GetOrdinal("Nombre"));
                    categoria.Imagen = lector.GetString(lector.GetOrdinal("Imagen"));
                }
            }
            comando.Dispose();
            return categoria;
        }

        public int Agregar(Categoria categoria)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Insert_Categoria");
            BaseDatos.AddOutParameter(comando,"IdCategoria",DbType.Int32, 4);
            BaseDatos.AddInParameter(comando,"Nombre", DbType.String,categoria.Nombre);
            BaseDatos.AddInParameter(comando, "Imagen", DbType.String, categoria.Imagen);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if(resultado == 0 ) throw new Exception("No se pudo insertar el registro en la base de datos");

            var valor = (int)BaseDatos.GetParameterValue(comando, "IdCategoria");
            comando.Dispose();

            return valor;
        }

        public bool Modificar(Categoria categoria)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Update_Categoria");
            BaseDatos.AddInParameter(comando,"IdCategoria",DbType.Int32, categoria.IdCategoria);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, categoria.Nombre);
            BaseDatos.AddInParameter(comando, "Imagen", DbType.String, categoria.Imagen);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if(resultado==0) throw new Exception("Hubo un error al modificar");

            comando.Dispose();

            return true;

        }

        public bool Eliminar(int idCategoria)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Delete_Categoria");
            BaseDatos.AddInParameter(comando, "IdCategoria", DbType.Int32, idCategoria);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if(resultado== 0) throw new Exception("No se pudo eliminar el registro");
            
            comando.Dispose();

            return true;
        }

        public int Count(string where)
        {
            var cantidad = 0;
            var comando = BaseDatos.GetStoredProcCommand("usp_Count_Categoria");
            BaseDatos.AddInParameter(comando, "FiltrosAdicionales", DbType.String, where);
            using (var dr = BaseDatos.ExecuteReader(comando))
            {
                if (dr.Read())
                {
                    cantidad = dr.GetInt32(dr.GetOrdinal("Cantidad"));
                }
            }


            comando.Dispose();
            return cantidad;
        }

        public IList<Categoria> Listar(int filasPorPagina, int pagina, int cantidad, string columnaOrden, string direccionOrden, string filtrosAdicionales)
        {
            var categorias = new List<Categoria>();
            var comando = BaseDatos.GetStoredProcCommand("usp_Search_Categoria");
            BaseDatos.AddInParameter(comando, "FilasPagina", DbType.Int32, filasPorPagina);
            BaseDatos.AddInParameter(comando, "Fila", DbType.Int32, pagina);
            BaseDatos.AddInParameter(comando, "Cantidad", DbType.Int32, cantidad);
            BaseDatos.AddInParameter(comando, "ColumnaOrden", DbType.String, columnaOrden);
            BaseDatos.AddInParameter(comando, "DireccionOrden", DbType.String, direccionOrden);
            BaseDatos.AddInParameter(comando, "FiltrosAdicionales", DbType.String, filtrosAdicionales);
           
  
            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                while (lector.Read())
                {
                    categorias.Add(new Categoria
                    {
                        IdCategoria = lector.GetInt32(lector.GetOrdinal("IdCategoria")),
                        Nombre = lector.GetString(lector.GetOrdinal("Nombre")),
                        Imagen = lector.IsDBNull(lector.GetOrdinal("Imagen")) ? string.Empty : lector.GetString(lector.GetOrdinal("Imagen"))
                    });
                }
            }
            comando.Dispose();
            return categorias;
        }
    }
}
