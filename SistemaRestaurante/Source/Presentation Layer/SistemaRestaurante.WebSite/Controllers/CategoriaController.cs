﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.BusinessLogic;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.WebSite.Controllers
{
    [Authorize]
    public class CategoriaController : Controller
    {
        //
        // GET: /Categoria/

        public ActionResult Index()
        {
            var listaCategorias = CategoriaBL.Instancia.Listar();
            return View("Listado",listaCategorias);
        }

      
        public ActionResult Create()
        {
            var categoria = new Categoria();

            return View("Create", categoria);
        }

        [HttpPost]
        public ActionResult Create(Categoria categoria)
        {
            //throw new Exception("Error!!! ");
            CategoriaBL.Instancia.Agregar(categoria);

            return Content("Registro Realizado", "text/html");
            // var listaCategorias = CategoriaBL.Instancia.Listar();
            //return View("Listado", listaCategorias);
        }

        public ActionResult Edit(int idCategoria)
        {
            var categoria = CategoriaBL.Instancia.Obtener(idCategoria);

            return View("Edit", categoria);

        }

        [HttpPost]
        public ActionResult Edit (Categoria categoria)
        {
            CategoriaBL.Instancia.Modificar(categoria);

            var listaCategorias = CategoriaBL.Instancia.Listar();
            return View("Listado", listaCategorias);
        }

        public ActionResult Delete (int idCategoria)
        {
            var categoria = CategoriaBL.Instancia.Obtener(idCategoria);

            return View("Delete", categoria);
        } 

        [HttpPost]
        public ActionResult Delete (Categoria categoria)
        {
            CategoriaBL.Instancia.Eliminar(categoria.IdCategoria);

            var listaCategorias = CategoriaBL.Instancia.Listar();
            return View("Listado", listaCategorias);
        }

        public ActionResult Details (int idCategoria)
        {
            var categoria = CategoriaBL.Instancia.Obtener(idCategoria);

            return View("Details", categoria);
        }

        
    }
}
