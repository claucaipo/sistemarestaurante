﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.BusinessLogic;

namespace SistemaRestaurante.WebSite.Controllers
{
    public class GraficoController : Controller
    {
        public ActionResult Grafico()
        {
            return View();
        }

        public FileResult CrearChart(string type)
        {
            var chartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), type);

            var pedidos = PedidoBL.Instancia.PedidosDiarios();

            var chart = new Chart();
            chart.Width = 700;
            chart.Height = 300;
            chart.BackColor = Color.FromArgb(211, 223, 240);
            chart.BorderlineDashStyle = ChartDashStyle.Solid;
            chart.BackSecondaryColor = Color.White;
            chart.BackGradientStyle = GradientStyle.TopBottom;
            chart.BorderlineWidth = 1;
            chart.Palette = ChartColorPalette.BrightPastel;
            chart.BorderlineColor = Color.FromArgb(26, 59, 105);
            chart.RenderType = RenderType.BinaryStreaming;
            chart.BorderSkin.SkinStyle = BorderSkinStyle.Emboss;
            chart.AntiAliasing = AntiAliasingStyles.All;
            chart.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal;
            chart.Titles.Add(CrearTitulo());
            chart.Legends.Add(CrearLeyenda());
            chart.Series.Add(CrearSeries(pedidos, chartType));
            chart.ChartAreas.Add(CrearChartArea());

            var ms = new MemoryStream();
            chart.SaveImage(ms);
            return File(ms.GetBuffer(), @"image/png");
        }

        [NonAction]
        public Title CrearTitulo()
        {
            var title = new Title();
            title.Text = "Grafico de Ventas diarias";
            title.ShadowColor = Color.FromArgb(32, 0, 0, 0);
            title.Font = new Font("Trebuchet MS", 14F, FontStyle.Bold);
            title.ShadowOffset = 3;
            title.ForeColor = Color.FromArgb(26, 59, 105);

            return title;
        }

        [NonAction]
        public Legend CrearLeyenda()
        {
            var legend = new Legend();
            legend.Name = "Resultado";
            legend.Docking = Docking.Bottom;
            legend.Alignment = StringAlignment.Center;
            legend.BackColor = Color.Transparent;
            legend.Font = new Font(new FontFamily("Trebuchet MS"), 9);
            legend.LegendStyle = LegendStyle.Row;

            return legend;
        }

        [NonAction]
        public Series CrearSeries(IList<Grafico> results, SeriesChartType chartType)
        {
            var seriesDetail = new Series();
            seriesDetail.Name = "Resultado";
            seriesDetail.IsValueShownAsLabel = false;
            seriesDetail.Color = Color.FromArgb(198, 99, 99);
            seriesDetail.ChartType = chartType;
            seriesDetail.BorderWidth = 2;
            seriesDetail["DrawingStyle"] = "Cylinder";
            seriesDetail["PieDrawingStyle"] = "SoftEdge";
            DataPoint point;

            foreach (var result in results)
            {
                point = new DataPoint();
                point.AxisLabel = result.Fecha;
                point.YValues = new double[] { Convert.ToDouble(result.Valor) };
                seriesDetail.Points.Add(point);
            }
            seriesDetail.ChartArea = "Resultado";

            return seriesDetail;
        }

        [NonAction]
        public ChartArea CrearChartArea()
        {
            var chartArea = new ChartArea();
            chartArea.Name = "Resultado";
            chartArea.BackColor = Color.Transparent;
            chartArea.AxisX.IsLabelAutoFit = false;
            chartArea.AxisY.IsLabelAutoFit = false;
            chartArea.AxisX.LabelStyle.Font = new Font("Verdana,Arial,Helvetica,sans-serif", 8F, FontStyle.Regular);
            chartArea.AxisY.LabelStyle.Font = new Font("Verdana,Arial,Helvetica,sans-serif", 8F, FontStyle.Regular);
            chartArea.AxisY.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.Interval = 1;
            
            //Para ponerlo en 3D
            // chartArea.Position.Width = 98;
            // chartArea.Position.Height = 70;
            // chartArea.Position.Y = 15;
            // chartArea.Position.X = 0;
            //chartArea.Area3DStyle.Enable3D = true;

            //Modificar parámetros en 3D
            /*chartArea.Area3DStyle.Rotation = 10;
            chartArea.Area3DStyle.Perspective = 10;
            chartArea.Area3DStyle.Inclination = 15;
            chartArea.Area3DStyle.IsRightAngleAxes=false;
            chartArea.Area3DStyle.WallWidth=0;
            chartArea.Area3DStyle.IsClustered=false;*/

            return chartArea;
        }

    }
}
