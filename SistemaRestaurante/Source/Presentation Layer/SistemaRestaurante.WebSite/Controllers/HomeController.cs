﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.WebSite.Filtros;

namespace SistemaRestaurante.WebSite.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        [OutputCache(Duration = 10)]
        [LogActionFilters]
        [Authorize]
        public ActionResult Index()
        {

            ViewBag.Message = DateTime.Now.ToString("T");

            return View();
        }


        public ContentResult ActualizarHora()
        {
            return Content(DateTime.Now.ToString("dd/MM/YYYY HH:mm:ss"), "text/html");
        }

        public ActionResult About()
        {

            //throw new DivideByZeroException();
            return View();
        }

        public ActionResult DevolverNombreProducto()
        {
            var producto = new Producto {Nombre = "PRoducto 1"};
           // return View("VistaProducto",producto);
            ViewBag.Clase = " Producto ";
            return View("VistaComun", producto);
        }

        public ActionResult DevolverNombreCategoria()
        {
            var categoria = new Categoria {Nombre = "Categoria demo"};
           // return View("VistaCategoria",categoria);
            ViewBag.Clase = " Categoria ";
            return View("VistaComun", categoria);
        }
    }
}
