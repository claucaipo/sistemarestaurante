﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.BusinessLogic;
using SistemaRestaurante.Utils.Enums;

namespace SistemaRestaurante.WebSite.Controllers
{
    public class PedidoController : Controller
    {
        public IList<DetallePedido> DetallePedidos
        {
            get { return Session["DetallePedido"] as List<DetallePedido>; }
            set { Session["DetallePedido"] = value; }

        } 
       public ActionResult Index()
       {
           var listados = PedidoBL.Instancia.Listar();
           return View(listados);
       }

        public ActionResult Create()
        {
            DetallePedidos = new List<DetallePedido>();

            var entidad = new Pedido
                              {
                                  Clientes = ClienteBL.Instancia.Listar(),
                                  Locales = LocalBL.Instancia.Listar(),
                                  DetallePedido = DetallePedidos,
                                  Fecha = DateTime.Now,
                              };
            PrepararDatos(ref entidad);
            return View(entidad);
        }

        public ActionResult CreateDetalle()
        {
            var detalle = new DetallePedido
                              {
                                  Productos = ProductoBL.Instancia.Listar(),
                                  NombreProducto = string.Empty,
                              };
            return PartialView(detalle);
        }

        [HttpPost]
        public PartialViewResult CreateDetalle(DetallePedido detalle)
        {
            var item = DetallePedidos.Count+1;
            var nombreProducto = ProductoBL.Instancia.Obtener(detalle.IdProducto).Nombre;
            detalle.NombreProducto = nombreProducto;
            detalle.IdItem = item;
            DetallePedidos.Add(detalle);
            return PartialView("ListadoDetalle", DetallePedidos);
        }

        public void PrepararDatos(ref Pedido pedido)
        {
            pedido.TiposPedidos = Utils.Utils.EnumToList<TipoPedido>();
            
        }
    }
}
