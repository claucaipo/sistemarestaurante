﻿
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SistemaRestaurante.WebSite.Core;
using SistemaRestaurante.BusinessLogic;

namespace SistemaRestaurante.WebSite.Controllers
{
    public class ReporteController : BaseController
    {
        //
        // GET: /Reporte/
        public ActionResult ReporteLocales()
        {
            return View("ReporteLocales");
        }


        public ActionResult ExportarLocales(string formato)
        {
            var datos = LocalBL.Instancia.Listar();
            RenderReport("rptLocales","DSLocal",datos,formato);
            return View("ReporteLocales");
        }

        public ActionResult GraficoJs()
        {
            return View();
        }

        public JsonResult ObtenerPorcentajeVentas()
        {
            var datos = PedidoBL.Instancia.PedidosDiarios();
            return Json(datos.ToArray(), JsonRequestBehavior.AllowGet);
        }
    }
}
